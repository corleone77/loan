#include <iostream>
#include <cmath>

using namespace std;

const double fee = 0.06;

int main()
{
  double x;
  int n;
  cin >> x >> n;
  cout << fixed << x*pow(1+fee, n) << endl;
  return 0;
}
